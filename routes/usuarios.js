/*
    Ruta: /api/usuarios
*/

const { Router } = require('express');
const { check } = require('express-validator');
const { validarCampos } = require('../middlewares/validar-campos');

const { getUsuarios, createUsuario, actualizarUsuario, borrarUsuario } = require('../controllers/usuarios');

const router = Router();

router.get('/', getUsuarios);
router.post('/',
    [
        check('nombre','El nombre es obligatorio').notEmpty(),
        check('password','La contraseña es obligatorio').notEmpty(),
        check('email','El email es obligatorio').isEmail(),
        validarCampos,
    ],
    createUsuario);

    router.put('/:id',
    [
        check('nombre','El nombre es obligatorio').notEmpty(),
        check('role','El role es obligatorio').isEmail(),
        check('email','El email es obligatorio').isEmail(),
        validarCampos,
    ],
    actualizarUsuario
    );

    router.delete('/id:',borrarUsuario);
module.exports = router;